package main

import (
	"context"
	"fmt"
	"log"
	
	"go.phucam.tv/store/pkg"
)

func main() {
	cnf := pkg.EtcdConfig{
		Endpoints:   []string{"localhost:2379"},
		DialTimeout: 0,
	}
	
	cli, err := cnf.GetClient()
	
	if nil != err {
		log.Fatalf("cannot get client: %v", err)
	} else {
		defer cli.Close()
	}
	
	ctx := context.Background()
	
	key := "tammy.playround.foo"
	_, _ = cli.Put(ctx, key, "value")
	
	if res, err := cli.Get(ctx, key); nil != err {
		log.Fatalf("cannot get config: %v", err)
	} else {
		fmt.Println("res.count: ", res.Count)
		fmt.Println("res.headers: ", res.Header)
		
		for i := range res.Kvs {
			kv := res.Kvs[i]
			fmt.Printf("res.kvs[%d].key: %s\n", i, kv.Key)
			fmt.Printf("res.kvs[%d].value: %s\n", i, string(kv.Value))
			fmt.Printf("res.kvs[%d].version: %s\n", i, kv.Version)
		}
	}
}
