package main

import (
	"context"
	"log"
	"net"
	"os"
	
	_ "github.com/mattn/go-sqlite3"
	"go.phucam.tv/ent"
	"go.phucam.tv/ent/proto/entpb"
	"go.phucam.tv/store/pkg"
	"google.golang.org/grpc"
)

// ---------------------
// Read
// ---------------------
//
// https://pkg.go.dev/google.golang.org/grpc#NewServer

func configPath() string {
	if len(os.Args) > 0 {
		return os.Args[1]
	}
	
	return ""
}

func main() {
	path := configPath()
	cnf, err := pkg.NewConfig(path)
	if err != nil {
		log.Fatalf("cannot create get: %v", err)
	}
	
	client, err := ent.Open(cnf.DB.Driver, cnf.DB.Url)
	if err != nil {
		log.Fatalf("failed opening connection to sqlite: %v", err)
	}
	defer client.Close()
	
	// Run the migration tool (creating tables, etc).
	if err := client.Schema.Create(context.Background()); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}
	
	server := buildServer(client, cnf)
	listener, err := net.Listen("tcp", cnf.GrpcServer.Address)
	if err != nil {
		log.Fatalf("failed net listener: %v", err)
	} else {
		log.Println("listen " + cnf.GrpcServer.Address)
	}
	
	if err = server.Serve(listener); nil != err {
		log.Fatalf("server error: %v", err)
	}
}

func buildServer(client *ent.Client, cnf *pkg.Config) *grpc.Server {
	opts := []grpc.ServerOption{
		grpc.MaxConcurrentStreams(cnf.GrpcServer.MaxConcurrentStream),
		// grpc.MaxRecvMsgSize(4),
		grpc.NumStreamWorkers(cnf.GrpcServer.NumStreamWorkers),
		grpc.ConnectionTimeout(cnf.GrpcServer.ConnectionTimeout),
	}
	server := grpc.NewServer(opts...)
	
	entpb.RegisterUserServiceServer(server, entpb.NewUserService(client))
	entpb.RegisterUserPasswordServiceServer(server, entpb.NewUserPasswordService(client))
	entpb.RegisterAccountServiceServer(server, entpb.NewAccountService(client))
	entpb.RegisterAccountFieldServiceServer(server, entpb.NewAccountFieldService(client))
	entpb.RegisterPortalServiceServer(server, entpb.NewPortalService(client))
	
	return server
}
