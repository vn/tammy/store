package pkg

import (
	"context"
	"fmt"
	"io/ioutil"
	"time"
	
	clientv3 "go.etcd.io/etcd/client/v3"
	env "go.phucam.tv/libs/env"
	"google.golang.org/grpc"
	"gopkg.in/yaml.v3"
)

func NewConfig(path string) (*Config, error) {
	raw, err := ioutil.ReadFile(path)
	if nil != err {
		return nil, fmt.Errorf("cannot read config file: %s. Error: %v", path, err)
	}
	
	parsedBytes := env.ReplaceEnvVariables(raw)
	config := &Config{}
	if err := yaml.Unmarshal(parsedBytes, &config); nil != err {
		return nil, fmt.Errorf("cannot decode configuration: %v", err)
	}
	
	return config, nil
}

type (
	Config struct {
		DB         DBConfig   `json:"dbs"`
		GrpcServer GrpcConfig `json:"grpc"`
	}
	
	DBConfig struct {
		Driver string
		Url    string
	}
	
	GrpcConfig struct {
		Address             string        `json:"address"`
		MaxConcurrentStream uint32        `json:"maxConcurrentStream"`
		NumStreamWorkers    uint32        `json:"numStreamWorkers"` // 0 (default) will disable workers and spawn a new goroutine for each stream.
		ConnectionTimeout   time.Duration `json:"connectionTimeout"`
	}
)

type EtcdConfig struct {
	Endpoints   []string
	DialTimeout time.Duration
}

func (c EtcdConfig) GetClient() (*clientv3.Client, error) {
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   c.Endpoints,
		DialTimeout: c.DialTimeout,
	})
	
	if err == context.DeadlineExceeded {
		return nil, err
	}
	
	// etcd clientv3 <= v3.2.9, grpc/grpc-go <= v1.2.1
	if err == grpc.ErrClientConnTimeout {
		return nil, err
	}
	
	return cli, err
}
