module go.phucam.tv/store

go 1.18

require (
	github.com/mattn/go-sqlite3 v1.14.13
	go.etcd.io/etcd/client/v3 v3.5.4
	go.phucam.tv/ent v0.0.0-20220729010622-d0dba5165abe
	go.phucam.tv/libs/env v0.0.0-20220803065333-d7f7261272c5
	google.golang.org/grpc v1.48.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

require (
	ariga.io/atlas v0.5.0 // indirect
	entgo.io/contrib v0.3.0 // indirect
	entgo.io/ent v0.11.1 // indirect
	github.com/agext/levenshtein v1.2.3 // indirect
	github.com/apparentlymart/go-textseg/v13 v13.0.0 // indirect
	github.com/coreos/go-semver v0.3.0 // indirect
	github.com/coreos/go-systemd/v22 v22.3.2 // indirect
	github.com/go-openapi/inflect v0.19.0 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/hashicorp/hcl/v2 v2.13.0 // indirect
	github.com/jhump/protoreflect v1.12.0 // indirect
	github.com/mitchellh/go-wordwrap v1.0.1 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/zclconf/go-cty v1.10.0 // indirect
	go.etcd.io/etcd/api/v3 v3.5.4 // indirect
	go.etcd.io/etcd/client/pkg/v3 v3.5.4 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.8.0 // indirect
	go.uber.org/zap v1.21.0 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/net v0.0.0-20220802222814-0bcc04d9c69b // indirect
	golang.org/x/sys v0.0.0-20220731174439-a90be440212d // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.12 // indirect
	google.golang.org/genproto v0.0.0-20220802133213-ce4fa296bf78 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
